<?php
/**
 * Controlador de tareas
 */
require_once('app/models/task.model.php');
require_once('app/views/task.view.php');

class TaskController {
    private $tareasModel;
    private $tareasView;

    public function __construct()
    {
        $this->tareasModel = new TaskModel();
        $this->tareasView = new TaskView();
    }


    private function checkSession() {

        session_start();

        if (empty($_SESSION['id'])) {
            header('Location:'.LOGIN);
        }
    }

    public function showHome() {
    
        $this->checkSession();

        $tareas = $this->tareasModel->getTareas();
        
        $this->tareasView->mostrarHome($tareas);

     }

     public function agregarTarea() {

        $this->checkSession();

        $descripcion = $_POST['descripcion'];
        $prioridad = $_POST['prioridad'];
    
        $this->tareasModel->insertarTarea($descripcion, $prioridad);
    
        header('Location: '.BASE_URL);
    }
    
    public function terminarTarea($tarea_id) {
        
        $this->checkSession();
    
        $this->tareasModel->actualizaTarea($tarea_id);
    
        header('Location: '.BASE_URL);
    }
    
    public function borrarTarea($tarea_id) {
        
        $this->checkSession();
        
        $this->tareasModel->eliminarTarea($tarea_id);
    
        header('Location: '.BASE_URL);
    }
}
