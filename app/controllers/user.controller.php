<?php
/**
 * Controlador de tareas
 */
require_once('app/models/user.model.php');
require_once('app/views/user.view.php');

class UserController {
    private $userModel;
    private $userView;

    public function __construct()
    {
        $this->userModel = new UserModel();
        $this->userView = new UserView();
    }

    public function logout() {
        session_start();
        session_destroy();
        header('Location: '.LOGIN);
    }

    public function showLogin($mensaje = '') {

        $this->userView->mostrarLogin($mensaje);
    }

    private function verificaUsuarioPass($userMail, $userPass)
    {
        // Traer de la base el usuario , del $userMail [mail, encriptado del password]
        // encriptar el $userPass y "compararlo" con el del usuario
        $user = $this->userModel->getUsuario($userMail);

        if (!empty($user) && password_verify($userPass, $user->pass))
        {
            session_start();
            $_SESSION['id'] = $user->id;
            $_SESSION['email'] = $user->email;

            return true;
        } else {
            return false;
        }
    }

    public function verificar() {
        $userMail = $_POST['email'];
        $userPass = $_POST['pass'];

        if ($this->verificaUsuarioPass($userMail, $userPass))
        {
            header('Location:'.HOME);
        } else 
        {
            $this->showLogin('Erorr de login');
        }

    }
        
}
