<?php
require_once('./libs/smarty-3.1.39/libs/Smarty.class.php');
/**
 * Realiza la visualizacion de los elementos de tareas
 */

 class TaskView {

    private $smarty;

    function __construct(){
        $this->smarty = new Smarty();
    }

    private function getActiveEmail() {
        session_start();
        $email = $_SESSION['email'];
        return $email;
    }

    public function mostrarHome($tareas) {
        $this->smarty->assign('titulo','Lista de Tareas');
        $this->smarty->assign('BASE_URL', BASE_URL);
        $this->smarty->assign('tareas',$tareas);
        $this->smarty->assign('email', $this->getActiveEmail());
        
        $this->smarty->assign('terminar', $this->mostrarTerminar($tareas));
        $this->smarty->assign('borrar', $this->mostrarBorrar($tareas));

        $this->smarty->display('templates/listTask.tpl');
    }

    public function mostrarLogin() {
        $this->smarty->assign('titulo','Todo-List');
        $this->smarty->assign('BASE_URL', BASE_URL);

        $this->smarty->display('templates/login.tpl');

    }


    private function mostrarSelector($tareas, $mostrarTodas = true) {
        $html = '<div class="form-group">';
        $html .= '<select name="tarea">';
        $html .= '<option default>-- Seleccion --</option>';
    
        foreach ($tareas as $tarea) {
            if ($mostrarTodas || ($tarea->terminada == 'N')) {
                $html .= '<option';
                $html .= ' value = "'.$tarea->id.'">';
                $html .= $tarea->descripcion;
                $html .= '</option>';
            }
        }
        $html .= '</select>';
        $html .= '</div>';
        return $html;
     }
    
    
    private function mostrarTerminar($tareas) {
        $html = '<div class="col-sm-4">';
        $html .= '<h3>Terminar Tarea</h3>';
        $html .= '<form action="'.BASE_URL.'terminarTarea" method="post">';
        $html .= '<label>Id Tarea: ';
        $html .= '</label>';
        $html .= $this->mostrarSelector($tareas, false);
        $html .= '<button type="submit" class="btn btn-success">Terminar</button>';    
        $html .= '</form>';
        $html .= '</div>';
        echo $html;
     }
    
     private function mostrarBorrar($tareas) {
        $html  = '<div class="col-sm-4">';
        $html .= '<h3>Borrar Tarea</h3>';
        $html .= '<form action="'.BASE_URL.'borrarTarea" method="post">';
        $html .= '<label>Id Tarea: ';
        $html .= '</label>';
        $html .= $this->mostrarSelector($tareas, true);
        $html .= '<button type="submit" class="btn btn-danger">Borrar</button>';        
        $html .= '</form>';
        $html .= '</div>';

        echo $html;
     }    
 }