<?php
require_once('./libs/smarty-3.1.39/libs/Smarty.class.php');
/**
 * Realiza la visualizacion de los elementos de tareas
 */

 class UserView {

    private $smarty;

    function __construct(){
        $this->smarty = new Smarty();
    }

    public function mostrarLogin($mensaje = '') {
        $this->smarty->assign('titulo','Todo-List');
        $this->smarty->assign('BASE_URL', BASE_URL);
        $this->smarty->assign('mensaje', $mensaje);

        $this->smarty->display('templates/login.tpl');

    }


 }