<?php
require_once('app/models/task.model.php');
require_once('api.view.php');

class ApiTaskController {

    private $model;
    private $view;
    private $data;
    
    public function __construct() {
        $this->model = new TaskModel();
        $this->view = new APIView();
        $this->data = file_get_contents("php://input");
    }

    public function obtenerTareas() {
        
        $sort = '';
        if (key_exists('sort', $_GET))
        {
            $sort = $_GET['sort'];
            if (key_exists('order', $_GET))
            {
                $sort = $sort . ' '.$_GET['order'];
            }  
            $sort = 'ORDER BY '.$sort;
        } else {
            $sort = 'ORDER BY prioridad';
        } 


        $tareas = $this->model->getTareas($sort);
        $this->view->response($tareas, 200);
    }

    public function obtenerTarea($params) {
        $id = $params[":ID"];
        
        if (key_exists(":COLS", $params)) 
        {
            $cols = $params[":COLS"];
        } else 
        {
            $cols = "*";
        }


        $tarea = $this->model->getTarea($id, $cols);
        $this->view->response($tarea, 200);
        
    }

    public function get_data() {
        return json_decode($this->data);
    }

    public function crearTarea() {

        $body = $this->get_data();

        $todoOk = $this->model->insertarTarea($body->descripcion, $body->prioridad) ;
        if ($todoOk)
        {
            $this->view->response("Se insertó correctamente", 200);
        } else {
            $this->view->response("Hubo un error", 500);
        }
        
    }

    public function updateTask($params = []) {
        $task_id = $params[':ID'];
        $task = $this->model->getTarea($task_id);

        if ($task) {
            $body = $this->get_data();
            $descripcion = $body->descripcion;
            $terminada = $body->terminada;
            $prioridad = $body->prioridad;

            $tarea = $this->model->actualizaCompleta($task_id, $descripcion, $terminada, $prioridad);
            $this->view->response("Tarea id=$task_id actualizada con éxito", 200);
        }
        else 
            $this->view->response("Task id=$task_id not found", 404);
    }   
    
    public function eliminaTarea($params = []) {
        $task_id = $params[':ID'];
        $task = $this->model->getTarea($task_id);

        if ($task) {
            $this->model->eliminarTarea($task_id);
            $this->view->response("Tarea id=$task_id eliminada con éxito", 200);
        }
        else 
            $this->view->response("Task id=$task_id not found", 404);
    }        
}