<?php
/**
 * Modelo de datos para Tareas
 */
require_once('model.php');
class TaskModel extends Model {
    
    /**
     * Devuelve un arreglo con todas las tareas.
     */
    public function getTareas($sort = '*') {
        
        $sql = "SELECT * FROM tarea $sort ";

        $stm = $this->pdo->prepare($sql);

        $stm->execute();

        $tareas = $stm->fetchAll(PDO::FETCH_OBJ);

        return $tareas;
    } 
    
    public function getTarea($id, $cols = '') {
        
        $sql = "SELECT $cols 
                    FROM tarea 
                    WHERE ID = ?";

        $stm = $this->pdo->prepare($sql);

        $stm->execute([$id]);

        $tareas = $stm->fetchAll(PDO::FETCH_OBJ);

        return $tareas;
    }    
    
    public function insertarTarea($descripcion, $prioridad) {

        $sql = "INSERT INTO tarea (descripcion, prioridad, terminada)
                VALUES (?, ?, 'N') ";

        $stm = $this->pdo->prepare($sql);

        $stm->execute([$descripcion, $prioridad]);

        return true;

    }

    public function actualizaCompleta($id, $descripcion, $terminada, $prioridad) {

        $sql = "UPDATE tarea 
                SET descripcion = ?, terminada = ?, prioridad = ? 
                WHERE id = ?";

        $stm = $this->pdo->prepare($sql);

        $stm->execute([$descripcion, $terminada, $prioridad, $id]);

    }

    public function actualizaTarea($tarea_id) {

        $sql = "UPDATE tarea 
                SET terminada = 'S' 
                WHERE id = ?";

        $stm = $this->pdo->prepare($sql);

        $stm->execute([$tarea_id]);

    }

    public function eliminarTarea($tarea_id) {

        $sql = "DELETE FROM tarea 
                WHERE id = ?";

        $stm = $this->pdo->prepare($sql);

        $stm->execute([$tarea_id]);

    }    

}

