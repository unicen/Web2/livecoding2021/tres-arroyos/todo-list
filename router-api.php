<?php
    require_once('libs/router/Router.php');
    require_once('app/api/task.api.controller.php');
    // crea el router
    $router = new Router();

    // define la tabla de ruteo
    $router->addRoute('tareas', 'GET', 'ApiTaskController', 'obtenerTareas');
    $router->addRoute('tareas', 'POST', 'ApiTaskController', 'crearTarea');
    $router->addRoute('tareas/:ID', 'GET', 'ApiTaskController', 'obtenerTarea');
    $router->addRoute('tareas/:ID/:COLS', 'GET', 'ApiTaskController', 'obtenerTarea');
    $router->addRoute('tareas/:ID', 'PUT', 'ApiTaskController', 'updateTask');
    $router->addRoute('tareas/:ID', 'DELETE', 'ApiTaskController', 'eliminaTarea');

    // rutea
    $router->route($_GET["resource"], $_SERVER['REQUEST_METHOD']);