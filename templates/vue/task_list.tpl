{literal}
<section id="template-vue-tasks">
    <h3> {{ subtitle }} </h3>

    <ul>
       <li v-for="task in tasks">
           <span v-if="task.terminada == 'S'"> <strike>{{ task.descripcion }} - {{task.prioridad}} </strike></span>
           <span v-else> {{ task.descripcion }} - {{task.prioridad}} </span> 

           <span v-if="task.terminada == 'N'">
                <a v-bind:href="'/eliminar/'+ task.id"> eliminar </a>
                <a v-bind:href="'/finalizar/'+ task.id"> finalizar </a>
           </span>
       </li> 
    </ul>
</section>
{/literal}
