<div class="row">
    <div class="col-sm-12">
    <h3>Crear Tarea</h3>
        {* <form id="form-tarea" action="'.{$BASE_URL}.'agregarTarea" method="post"> *}
        <form id="form-tarea" method="post">
            <div class="form-group">
                <label class="control-label">Descripcion: </label><input type="text" name="descripcion" id="descripcion">
            </div>
            <div class="form-group">
                <label class="control-label">Prioridad: </label><input type="number" name="prioridad" id="prioridad">
            </div>
            <button type="submit" class="btn btn-success">Crear</button>
        </form>
    </div>
</div>
<script src="js/formulario.js"></script>
{include file="templates/vue/task_list.tpl"}
<script src="js/tareas_vs.js"></script>
<br>

