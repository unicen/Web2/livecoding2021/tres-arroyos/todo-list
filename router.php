<?php
define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');
define('HOME', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/home');
define('LOGIN', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/login');

require_once('app/controllers/task.controller.php');
require_once('app/controllers/user.controller.php');

$tareasControllers = new TaskController();
$userControllers = new UserController();


if (!empty($_GET['action'])){
    $accion = $_GET['action'];
}
else {
    $accion = 'home';
}

$params = explode('/',$accion);

switch ($params[0]){
    case 'login':
        $userControllers->showLogin();
        break;   
    case 'verificar':
        $userControllers->verificar();
        break;    
    case 'logout':
        $userControllers->logout();
        break;               
    case 'home':
        $tareasControllers->showHome();
        break;
    case 'agregarTarea':
        $tareasControllers->agregarTarea();
        break;
    case 'terminarTarea':
        $tareasControllers->terminarTarea($params[1]);
        break;
    case 'borrarTarea':
        $tareasControllers->borrarTarea($params[1]);
        break;
    default:
        echo('404 page not found');
        break;
}



