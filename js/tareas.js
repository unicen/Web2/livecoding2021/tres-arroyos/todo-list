function createTaskHTML(task) {
    let element = `${task.descripcion}: ${task.prioridad}`;
    
    if ((task.terminada == '1') || (task.terminada == 'S'))
        element = '<strike>'+element+'</strike>';
    else {
        element += ' <a href="tarea/'+task.id+'">Ver</a> ';
        element += ' <a href="terminarTarea/'+task.id+'">Finalizar</a> ';
        element += ' <a href="borrarTarea/'+task.id+'">Eliminar</a>';
    }
        
    element = '<li>'+element+'</li>';
    return element;  
}


function getTasks() {
    fetch('api/tareas/')
    .then(response => response.json())
    .then(tasks => {
        let content = document.querySelector(".lista-tareas");
        content.innerHTML = "";
        for(let task of tasks) {
            content.innerHTML += createTaskHTML(task);
        }
    })
    .catch(error => console.log(error));
}



