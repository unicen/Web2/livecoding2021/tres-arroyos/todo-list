document.getElementById("form-tarea").addEventListener('submit', addTask);

function addTask(e) {
    e.preventDefault();
    
    console.log('Pasa');

    let data = {
        descripcion:  document.querySelector("input[name=descripcion]").value,
        prioridad:  document.querySelector("input[name=prioridad]").value
    }

    fetch('api/tareas', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},       
        body: JSON.stringify(data) 
     })
     .then(response => {
         getTasks();
     })
     .catch(error => console.log(error));
}
